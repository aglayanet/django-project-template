{% if False %}
=======================
Django project template
=======================

Использование:
--------------

1. Создать виртуальное окружение и активировать его
2. Установить в окружение Django>=1.8
3. Выполнить ``django-admin.py startproject --template https://bitbucket.org/aglayanet/django-project-template/get/master.zip --extension py,md,rst,sample название_проекта``
{% endif %}



==============================
{{ project_name }}
==============================


Локальная разработка:
---------------------

1. Установить ``vagrant`` и ``virtualbox``
2. Выполнить ``vagrant up`` в корне проекта

Проект станет доступен по адресу http://127.0.0.1:8000

Рекомендуемая структура проекта:
--------------------------------

::

    .
    └── home
        └── {{ project_name }}
            ├── site - папка с проектом
            ├── uwsgi.ini
            └── venv - папка с виртуальным окружением



Рекомендуемый способ запуска проекта:
-------------------------------------

``supervisor <-> uwsgi <-> django``

Примеры файлов с настройками для ``supervisor`` и ``uwsgi`` находятся в папке ``conf``

Развёртывание проекта:
----------------------

Все команды выполняются от имени пользователя {{ project_name }}

0. Клонирование проекта ``git clone $PROJECT_REPO ~/site/``
1. Установка виртуального окружения ``virtualenv ~/venv/``
2. Активация виртуального окружения ``. ~/venv/bin/activate``
3. Переходим в папку с проектом ``cd ~/site/``
4. Установка зависимостей проекта в виртуальное окружение ``pip install -r requirements.txt``
5. Собираем статику ``./manage.py collectstatic --noinput``
6. Выполняем миграции ``./manage.py migrate``
7. Выполняем перевод сообщений ``./manage.py compilemessages``
8. Копируем в корень домашней папки пользователя {{ project_name }} файл ``conf/uwsgi.ini.sample``
9. Переименовываем ``~/uwsgi.ini.sample`` в ``~/uwsgi.ini`` и вносим необходимые изменения
10. Деактивируем виртуальное окружение при помощи команды ``deactivate``
11. Проверяем правильность настройки проекта при помощи ``uwsgi ~/uwsgi.ini``


Настройка ``supervisor``, ``nginx`` и установка system-wide uwsgi оставим за кадром.
Примеры файлов с настройками для ``supervisor`` и ``nginx`` находятся в папке ``conf``.