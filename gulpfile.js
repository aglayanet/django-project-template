var gulp = require('gulp');
var runSeq = require('run-sequence');

require('./tasks/jspm');
require('./tasks/sass');
require('./tasks/server');
require('./tasks/static');

gulp.task('default', function (done) {
    return runSeq(['watch:sass', 'watch:js'], 'server', done);
});

gulp.task('build', function (done) {
    return runSeq(['build:css', 'build:js', 'build_landing:js', 'copy:static'], done);
});