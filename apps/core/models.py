# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _


class GenericModel(models.Model):
    created_at = models.DateTimeField(
        verbose_name=_('created'),
        auto_now_add=True, db_index=True)

    updated_at = models.DateTimeField(
        verbose_name=_('updated'),
        auto_now=True, db_index=True)

    class Meta:
        abstract = True
        ordering = ['created_at', 'updated_at']
        get_latest_by = 'created_at'
