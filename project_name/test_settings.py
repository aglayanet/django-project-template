# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from {{ project_name }}.settings import *

DEBUG = True
FIXTURE_DIRS = [os.path.join(BASE_DIR, 'fixtures')]
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
        'TEST_NAME': ':memory:'
    }
}
