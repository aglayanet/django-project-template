var path = require('path');
var gulp = require('gulp');

var runSeq = require('run-sequence');

var sassJspm = require('sass-jspm-importer');
var plugins = require('gulp-load-plugins')();

var config = require('../package.json');

var jspm_dirs = config.jspm.directories;
var dirs = config.directories || {};


var JSPM_DIR = path.join(jspm_dirs.baseURL || '/', 'jspm_packages');
var SCSS_MASK = path.join(dirs.assets._root || 'assets', dirs.assets.scss, '/**/*.scss');
var CSS_DEST = path.join(dirs.static._root || 'static', dirs.static.css);

var BUILD_DEST = path.join(dirs.build._root || 'build', dirs.static.css);

gulp.task('sass', function () {
    return gulp.src(SCSS_MASK)
        .pipe(plugins.plumber())

        // Init sourcemap
        .pipe(plugins.sourcemaps.init())

        // Sass compiler
        .pipe(plugins.sass({
            sourcemap: true,
            errLogToConsole: true,
            style: 'expanded',
            functions: sassJspm.resolve_function(JSPM_DIR),
            importer: sassJspm.importer
        }).on('error', plugins.util.log))

        // Postcss processor
        .pipe(plugins.postcss([
            require('autoprefixer')({
                browsers: ['> 1%', 'IE 9']
            })
        ]).on('error', plugins.util.log))

        // Write sourcemap
        .pipe(plugins.sourcemaps.write({
            includeContent: true
        }))
        .pipe(gulp.dest(CSS_DEST))
        .pipe(plugins.livereload());
});

gulp.task('watch:sass', function () {
    return gulp.watch(SCSS_MASK, ['sass']);
});

gulp.task('minify', function () {
    gulp.src(path.join(CSS_DEST, '/*.css'))
        .pipe(plugins.minifyCss())
        .pipe(gulp.dest(BUILD_DEST));
});

gulp.task('build:css', function () {
    return runSeq('sass', 'minify');
});
