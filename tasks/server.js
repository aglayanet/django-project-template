var gulp = require('gulp');
var http = require('http');
var st = require('st');

var plugins = require('gulp-load-plugins')();

gulp.task('server', function(done) {
    plugins.livereload.listen(8081);

    gulp.watch('*.html', function (e) {
        gulp.src(e.path)
            .pipe(plugins.livereload());
    });

    http.createServer(
        st({
            path: process.cwd(),
            index: false,
            cache: false
        })
    ).listen(8080, done);
});