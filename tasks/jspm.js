var path = require('path');
var gulp = require('gulp');

var plugins = require('gulp-load-plugins')();

var config = require('../package.json');

var jspm_dirs = config.jspm.directories;
var dirs = config.directories || {};

var JS_DIR =  path.join(dirs.static._root || 'static', dirs.static.js);
var JS_MASK = path.join(JS_DIR, '/**/*.js');

var BUILD_DEST = path.join(dirs.build._root || 'build', dirs.static.js);

gulp.task('watch:js', function () {
    return gulp.watch(JS_MASK, function (e) {
        return gulp.src(e.path)
            .pipe(plugins.livereload());
    });
});

gulp.task('build:js', function () {
    return gulp.src(path.join(JS_DIR, '/main.js'))
        .pipe(plugins.jspm({selfExecutingBundle: true}))
        .pipe(plugins.uglify({
            preserveComments: 'license'
        }))
        .pipe(gulp.dest(BUILD_DEST));
});

gulp.task('build_landing:js', function () {
    return gulp.src(path.join(JS_DIR, '/landing.js'))
        .pipe(plugins.jspm({selfExecutingBundle: true}))
        .pipe(plugins.uglify({
            preserveComments: 'license'
        }))
        .pipe(gulp.dest(BUILD_DEST));
});

gulp.task('build_dev:js', function () {
    return gulp.src(path.join(JS_DIR, '/main.js'))
        .pipe(plugins.jspm({
            selfExecutingBundle: true,
            arithmetic: '+ js/mocks'
        }))
        .pipe(plugins.uglify({
            preserveComments: 'license'
        }))
        .pipe(gulp.dest(BUILD_DEST));
});