var gulp = require('gulp');

var config = require('../package.json');

var dirs = config.directories || {};

var BUILD_DEST = dirs.build._root || 'build';


gulp.task('copy:static', function () {
    return gulp.src([
        'static/fonts/**/*',
        'static/favicons/**/*',
        'static/img/**/*',
        'static/media/**/*'
    ], {
        base: 'static'
    }).pipe(gulp.dest(BUILD_DEST));
});